console.log("Hi, B248!");
console.log(document); //result -html document
console.log(document.querySelector("#txt-first-name"));
console.log(document.querySelector("#txt-last-name"));
/*
	document- refers the ehole web page
	querySelector - used  to selct a specific elemnt (obj) as long asit is inde the html tag (HTML Element)
	-takes a string input that is formatted like CSS selector 
	-can slect elements reagrdless if the string is an ID, class, or a tag as long as the leemnt is existing in the webpage
	
*/
/*
	Alternative methods that we use aside from the querySelector in retrieving elements
	Syntax:
	document.getElementById()
	document.getElementByClassName()
	document.getElementbyTagNAme()
	
*/



/*txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
})

txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtLastName.value
})*/

//Activity
function updateFullName(){

	const fullName= txtFirstName.value + ' ' + txtLastName.value;
	spanFullName.innerHTML = fullName.trim();
}
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener('keyup', (event)=>{
	updateFullName();
});
txtLastName.addEventListener('keyup', (event)=>{
	updateFullName();
});
